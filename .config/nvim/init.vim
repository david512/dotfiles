" neovim-config
"                         _
"   _ __   ___  _____   _(_)_ __ ___
"  | '_ \ / _ \/ _ \ \ / / | '_ ` _ \
"  | | | |  __/ (_) \ V /| | | | | | |
"  |_| |_|\___|\___/ \_/ |_|_| |_| |_|
"

" ============================================================================

" Vim-plug first time ininitialization
" NOTE! Install dependencies first: 
" sudo apt install curl git python3 python3-pip 
" pip3 install --user --upgrade pynvim
" Optional: sudo apt install fonts-powerline

let vim_plug_just_installed = 0
let vim_plug_path = expand('~/.local/share/nvim/site/autoload/plug.vim')
if !filereadable(vim_plug_path)
    echo "Installing Vim-plug..."
    echo ""
    !mkdir -p ~/.local/share/nvim/site/autoload && wget -O ~/.local/share/nvim/site/autoload/plug.vim https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
"    !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    let vim_plug_just_installed = 1
endif

" manually load vim-plug the first time
if vim_plug_just_installed
    :execute 'source '.fnameescape(vim_plug_path)
endif



" ===========================================
"           Plugins initialization
" ===========================================

call plug#begin()

" Asynchronous Lint Engine
Plug 'https://github.com/dense-analysis/ale'

" Autocomplete 
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
"   Plug 'https://github.com/deoplete-plugins/deoplete-clang'
"   Plug 'https://github.com/tweekmonster/deoplete-clang2'
 	Plug 'Shougo/deoplete-clangx'
    Plug 'https://github.com/Shougo/neoinclude.vim'
    Plug 'https://github.com/Shougo/neco-vim'
"	Plug 'https://github.com/deoplete-plugins/deoplete-jedi'
"Plug 'ycm-core/YouCompleteMe'
"Plug 'sheerun/vim-polyglot'

" Snippets
Plug 'https://github.com/SirVer/ultisnips'
    Plug 'https://github.com/honza/vim-snippets'

" Autopairs
Plug 'Raimondi/delimitMate'
"Plug 'jiangmiao/auto-pairs'

" Syntax
"Plug 'https://github.com/vim-python/python-syntax'
"Plug 'https://github.com/bfrg/vim-cpp-modern'

" Fuzzy finder
"Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
"   Plug 'https://github.com/junegunn/fzf.vim'

" interface (cli)
"Plug 'itchyny/lightline.vim'
Plug 'vim-airline/vim-airline'
Plug 'ryanoasis/vim-devicons'

"Arduino"
"Plug 'stevearc/vim-arduino'
"Plug 'sudar/vim-arduino-syntax'
"Plug 'z3t0/arduvim'

call plug#end()


"====================================
"              Settings
"====================================

"let g:ale_completion_enabled = 1
"set omnifunc=ale#completion#OmniFunc

" Basic stuff
syntax on
filetype plugin indent on
"set autoindent
"set smarttab
"set number relativenumber
"set clipboard=unnamedplus
"set inccommand=nosplit
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
"set ignorecase
"set smartcase
"set undofile
"set backspace=indent,eol,start
set number
set mouse=a
set incsearch
set hlsearch
set encoding=UTF-8

" Colorscheme
"hi LineNr              ctermfg=3
"hi CursorLineNr        ctermfg=3
"hi Statement           ctermfg=3
"hi Visual           ctermbg=8
"hi Search           ctermbg=8
"hi SpellBad         ctermbg=1   ctermfg=0   cterm=underline
"hi SpellCap         ctermbg=2   ctermfg=0   cterm=underline
"hi SpellRare        ctermbg=3   ctermfg=0   cterm=underline
"hi SpellLocal       ctermbg=5   ctermfg=0   cterm=underline
"hi Pmenu            ctermbg=0   ctermfg=4
"hi PmenuSel         ctermbg=8   ctermfg=4
"hi PmenuSbar        ctermbg=0
"hi DiffAdd          ctermbg=2   ctermfg=0
"hi DiffChange       ctermbg=4   ctermfg=0
"hi DiffDelete       ctermbg=1   ctermfg=0
"hi Folded           ctermbg=8
"hi FoldColumn       ctermbg=8
"hi SignColumn       ctermbg=0   ctermfg=7
"hi ALEWarning       ctermbg=3   ctermfg=0
hi ALEErrorSign     ctermbg=1   ctermfg=15
hi ALEError         ctermbg=1   ctermfg=15

" Python paths, needed for virtualenvs
let g:python3_host_prog = '/usr/bin/python3'
let g:python_host_prog = '/usr/bin/python2'


"====================================
"           Plugin configs
"====================================

" Airline - statusline
set noshowmode
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1 "disable if your teminal dont support powerline fonts 

"set laststatus=2   "Lightline only

" Deoplete - autocomletion
let g:deoplete#enable_at_startup = 1
"let g:deoplete#enable_at_startup = 0
autocmd InsertEnter * call deoplete#enable() " Enable deoplete when InsertEnter.
"call deoplete#custom#source('clang', 'fileytpes', ['c', 'cpp', 'arduino'])
"set completeopt-=preview
"if !exists('g:deoplete#omni#input_patterns')
"  let g:deoplete#omni#input_patterns = {}
"endif

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

" Delimit-mate
let delimitMate_expand_cr = 1
" vim-arduino
autocmd BufRead,BufNewFile,BufReadPost *.ino,*.pde set filetype=cpp
"autocmd BufNewFile,BufRead *.ino let g:airline_section_x='%{MyStatusLine()}'


"===================================
"           Key Bindings
"===================================

" Leader key
"let mapleader = ' '

" Complete with <TAB>
"function! s:check_back_space() abort "{{{
"  let col = col('.') - 1
"  return !col || getline('.')[col - 1]  =~ '\s'
"endfunction"}}}
"inoremap <silent><expr> <TAB>
"      \ pumvisible() ? "\<C-n>" :
"      \ <SID>check_back_space() ? "\<TAB>" :
"      \ deoplete#manual_complete()

" UltiSnips
let g:UltiSnipsExpandTrigger='<tab>' "'<c-q>' 
let g:UltiSnipsJumpForwardTrigger='<c-s>' "'<c-b>'
let g:UltiSnipsJumpBackwardTrigger='<c-z>' "'<c-z>'

" When line overflows, it will go
" one _visual_ line and not actual
"map j gj
"map k gk
"map <Down> g<Down>
"map <Up> g<Up>

" Insert one character
"nnoremap <C-i> i_<Esc>r
nnoremap <Space> i_<Esc>r

" Shifting rows up and down
nnoremap <S-Down> :m .+1<CR>==
nnoremap <S-Up> :m .-2<CR>==
inoremap <S-Down> <Esc>:m .+1<CR>==gi
inoremap <S-Up> <Esc>:m .-2<CR>==gi
vnoremap <S-Down> :m '>+1<CR>gv=gv
vnoremap <S-Up> :m '<-2<CR>gv=gv

" Switch between buffors
noremap <Leader><Tab> :bn<CR>
noremap <Leader><S-Tab> :bp<CR>
nmap <leader>1 :b1<CR> 
nmap <leader>2 :b2<CR> 
nmap <leader>3 :b3<CR> 
nmap <leader>4 :b4<CR> 
nmap <leader>5 :b5<CR> 
nmap <leader>6 :b6<CR> 
nmap <leader>7 :b7<CR> 
nmap <leader>8 :b8<CR> 
nmap <leader>9 :b9<CR> 
nmap <leader>10 :b10<CR>

